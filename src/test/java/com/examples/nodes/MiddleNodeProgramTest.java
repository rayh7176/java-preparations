package com.examples.nodes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.ArgumentConverter;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.MatcherAssert.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MiddleNodeProgramTest {

    private MiddleNodeProgram middleNodeProgram;

    @BeforeEach
    public void init(){
        System.out.println("Start the class view");
        middleNodeProgram = new MiddleNodeProgram();
    }

    @ParameterizedTest
    @MethodSource("populatedProperties")
    public void testFindMiddleNode(@ConvertWith(ArrayStringConverter.class) String [] elems){
        int expectedMiddleValue = 7;
        for (int i =0; i<elems.length; i++)
            middleNodeProgram.populate(elems[i]);
        int middleReceived = middleNodeProgram.findMiddleNode();
        assertThat(middleReceived, is(expectedMiddleValue));
    }

    private Stream<Arguments> populatedProperties(){
        return Stream.of(Arguments.of(new String[] {"1", "2","2","2","2","2","2","2","2","2"}));
    }

    private class ArrayStringConverter implements ArgumentConverter {

        @Override
        public Object convert(Object source, ParameterContext parameterContext) throws ArgumentConversionException {
            if (!(source instanceof String)) {
                throw new IllegalArgumentException(
                        "The argument should be a string: " + source);
            }
            try {
                String[] parts = ((String[]) source);
                return parts;
            } catch (Exception e) {
                throw new IllegalArgumentException("Failed to convert", e);
            }
        }

    }

}
