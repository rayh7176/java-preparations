package com.examples.threads;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ArrayBlockingQueueProgramTest {

    private ArrayBlockingQueueProgram<String> arrayBlockingQueueProgram;

    @BeforeEach
    public void init(){
        arrayBlockingQueueProgram = new ArrayBlockingQueueProgram(5);
    }

    @ParameterizedTest
    @MethodSource("dataToStore")
    public void testStoreData(String data1, String data2, String data3, String data4, String data5){
        arrayBlockingQueueProgram.offerData(data1);
        arrayBlockingQueueProgram.offerData(data2);
        arrayBlockingQueueProgram.offerData(data3);
        arrayBlockingQueueProgram.offerData(data4);
        arrayBlockingQueueProgram.offerData(data5);

        while(arrayBlockingQueueProgram.getCounter() > 0)
            System.out.println(arrayBlockingQueueProgram.pullData());
    }

    private Stream<Arguments> dataToStore(){
        return Stream.of(Arguments.of("1", "2", "3", "4", "5"));
    }
}
