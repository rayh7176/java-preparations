package com.examples.prices;

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LatestPricesTest {

    private LatestPrices latestPrices;

    @BeforeEach
    public void init(){
        System.out.println("Started the test");
    }

    @AfterEach
    public void finished(){
        System.out.println("Finished the test");
    }

    @Test
    public void testReceivePrice(){
        latestPrices = new LatestPrices();
        // Have multiple threads calling it
        new Thread( () -> latestPrices.receivePrice(new LatestPrices.Rate(1, "EUR/USD"))).start();
        new Thread( () -> latestPrices.receivePrice(new LatestPrices.Rate(2, "EUR/USD"))).start();
        new Thread( () -> latestPrices.receivePrice(new LatestPrices.Rate(6, "EUR/USD"))).start();
        new Thread( () -> latestPrices.receivePrice(new LatestPrices.Rate(6, "EUR/USD"))).start();
        new Thread( () -> latestPrices.receivePrice(new LatestPrices.Rate(5, "EUR/USD"))).start();
        new Thread( () -> latestPrices.receivePrice(new LatestPrices.Rate(4, "EUR/USD"))).start();
        new Thread( () -> latestPrices.receivePrice(new LatestPrices.Rate(3, "EUR/USD"))).start();
        new Thread( () -> latestPrices.receivePrice(new LatestPrices.Rate(7, "EUR/USD"))).start();
        new Thread( () -> latestPrices.receivePrice(new LatestPrices.Rate(8, "EUR/USD"))).start();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
