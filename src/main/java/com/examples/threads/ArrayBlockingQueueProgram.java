package com.examples.threads;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ArrayBlockingQueueProgram<T> {
    private ReentrantLock reentrantLock = new ReentrantLock();
    private Condition readCondition = reentrantLock.newCondition();
    private Condition writeCondition = reentrantLock.newCondition();
    private Object [] store;
    private int counter = 0;

    public ArrayBlockingQueueProgram(int size){
        store =  new Object[size];
    }

    public int getCounter(){
        return counter;
    }

    public void offerData(T data){
        try {
            reentrantLock.lock();
            if (counter > store.length)
                writeCondition.await();

            store[counter++] = data;
            readCondition.signal();
        }catch(Exception ex) {
            ex.printStackTrace();
        }
        finally {
            reentrantLock.unlock();
        }
    }

    public T pullData(){
        T data = null;
        try {
            reentrantLock.lock();

            if (counter == 0)
                readCondition.await();

            data = (T) store[counter-1];
            if (counter >=1)
                counter--;

            writeCondition.signal();
            return data;

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            reentrantLock.unlock();
        }
        return data;
    }

}
