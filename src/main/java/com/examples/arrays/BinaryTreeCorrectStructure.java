package com.examples.arrays;



import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class BinaryTreeCorrectStructure {
    public static boolean returnCorrectStructure(Node root){

        if ( root == null)
            return false;

        Queue<Node> nodes = new ArrayBlockingQueue<Node>(5);

        nodes.offer(root);
        boolean notCompleteTree = false;

        //Build a STACK and not a FIFO.
        while (!nodes.isEmpty()) {
            Node node = nodes.poll();
            if (node.left != null) {
                if (notCompleteTree == true) {
                    return false;
                }
                nodes.offer(node.left);
            }
            else
                // Assuming if one of the tree structure is incomplete
                notCompleteTree = true;

            if (node.right != null){
                if (notCompleteTree == true) {
                    return false;
                }
                nodes.offer(node.right);
            }
            else
                // Assuming if one of the tree structure is incomplete
                notCompleteTree = true;
        }

        return false;

    }

    static public class Node {

        private final int data;
        public Node left;
        public Node right;

        public Node(final int data){
            this.data = data;
        }
    }

}
