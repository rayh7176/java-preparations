package com.examples.arrays;

import java.util.Map;
import java.util.stream.LongStream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public class FactorialProgram {
    static public long factorialStreams(int n) {
        if (n > 20) throw new IllegalArgumentException(n + " is out of range");
        return LongStream.rangeClosed(2, n).reduce(1, (a, b) ->  a*b);
    }

    public long factorial(int n) {
        if (n > 20) throw new IllegalArgumentException(n + " is out of range");
        return (1 > n) ? 1 : n * factorial(n - 1);
    }

    public long factorialImperative(int n) {
        if (n > 20) throw new IllegalArgumentException(n + " is out of range");
        long product = 1;
        for (int i = 2; i < n; i++) {
            product *= i;
        }
        return product;
    }

    public final static void main(String args []){
        System.out.println("Factorial::4*3*2*1::"+factorialStreams(4));
        System.out.println("Factorial::stream::"+new FactorialProgram().m);
    }

    Map<Long, Long> m = LongStream.range(1, 20)
            .boxed()
            .collect(toMap(identity(), this::magic));

    private <U, T> U magic(T t) {
            return null;
    }

}
