package com.examples.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PairLargestsSums {
    public static List<String> pairNumbers(int arr [], int k){
        // Sort the given array
        Arrays.sort(arr);
        int position = 0;
        // Find the max value near K
        for (int d=0; d < arr.length; d++) {
            if (arr[d] >= k) {
                position = d;
                break;
            }
        }
        int maximum = 0;
        List<String> data = new ArrayList<>();
        // devide and conquer but that is for sorting
        for (int i=0; i< position ; i++) {
            for(int j= i+1; j < position ; j++) {
                int sum = arr[i] + arr[j];
                if (arr[i] + arr[j] < k &&  arr[i] + arr[j] > maximum) {
                    data.add(String.valueOf(i + " " + j));
                    maximum = arr[i] + arr[j];
                }
            }
        }
        return data;
    }

    public static void main(String args [] ){
        int [] dd = new int [] {4,45,56,1,34,67,47, 23, 12};
        List<String> mm = pairNumbers(dd, 65);
        System.out.println("DATA="+mm);
    }
}
