package com.examples.arrays;

public class ValidateBinaryTree {
    public static boolean validateBinary(Node a, Node b){
        if ((a == null && b == null) || (a != null && b!= null)
                && (a.data == b.data)
                && validateBinary(a.left, b.left)
                && validateBinary(a.right, b.right))
            return true;
        else
            return false;
    }

    static public class Node {

        private final int data;
        public Node left;
        public Node right;

        public Node(final int data){
            this.data = data;
        }
    }

    public static void main(String args []) {
        Node rootA = new Node(100);
        rootA.left = new Node(99);
        rootA.left.left = new Node(98);
        rootA.left.right = new Node(97);
        rootA.right = new Node(96);
        rootA.left.left = new Node(95);
        rootA.left.right = new Node(94);

        Node rootB = new Node(100);
        rootB.left = new Node(99);
        rootB.left.left = new Node(98);
        rootB.left.right = new Node(97);
        rootB.right = new Node(96);
        rootB.left.left = new Node(95);
        rootB.left.right = new Node(93);

        boolean results = validateBinary(rootA, rootB);
    }
}
