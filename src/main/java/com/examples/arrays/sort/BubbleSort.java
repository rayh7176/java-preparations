package com.examples.arrays.sort;

public class BubbleSort {

    /**
     * SortElements
     *
     * @param array
     * @return
     */
    public static int[] sortArray(int array[]) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 1; j < array.length - 1; i++) {
                if (array[i] < array[j]) {
                    // swap elements
                    int tmp = array[j];
                    array[j] = array[i];
                    array[i] = tmp;
                }
            }
        }
        return array;
    }

}
