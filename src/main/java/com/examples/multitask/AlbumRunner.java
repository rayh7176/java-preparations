package com.examples.multitask;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AlbumRunner {

    static List<String> getAllAlbumsByName(List<Band> bands){
        return bands.stream().
                map(b -> b.albums).
                flatMap(a -> a.stream()).
                map(c -> c.name).
                collect(Collectors.toList());
    }

    static Map<String, List<Album>> getAllAlbumsByBandName(List<Band> bands){
         return bands.stream().
                collect(Collectors.toMap(band -> band.name, band -> band.albums)
                );
    }

}
