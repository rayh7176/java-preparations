package com.examples.prices;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

/**
 * Always shows latest prices
 */
public class LatestPrices {

    private ConcurrentHashMap<String, Integer> store;
    private CompletableFuture<Integer> completableFuture = new CompletableFuture<>();

    public LatestPrices() {
        this.store = new ConcurrentHashMap<>();
    }

    public void receivePrice(Rate rate) {
        // validate rate
        store.put(rate.inst, rate.price);
        completableFuture.supplyAsync(populatePrices(rate)).thenAccept(p -> publishLatest(p));
    }

    private Supplier<Integer> populatePrices(Rate rate){
        return () -> {
            int rateInStore = store.get(rate.inst);
            if (rateInStore < rate.price) {
                store.computeIfPresent(rate.inst, (v1,v2) ->  v2);
                return rate.price;
            } else {
                return rateInStore;
            }
        };
    }

    private void publishLatest(int value){
        System.out.println("published value price="+value);
    }


    static class Rate {
        private int price;
        private String inst;

        public Rate(int price, String inst) {
            this.price = price;
            this.inst = inst;
        }
    }

}
